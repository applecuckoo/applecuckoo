# applecuckoo

This is a 'satellite' account that's only used to contribute to other projects. My code is hosted on [GitHub](https://github.com/applecuckoo).